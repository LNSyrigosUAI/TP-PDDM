using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntuacion : MonoBehaviour
{
    public int _Puntuacion = 0;
    public Text puntuacionText;
    public int puntuacionJugador2 = 0;
    public Text PuntuacionTextJ2;


    /// <summary>
    /// Esta funci�n se ejecuta al inicio del juego. Verifica si puntuacionText es nulo y muestra un mensaje de error en la consola de Unity si no se asign� correctamente.
    /// </summary>
    void Start()
    {
        if (puntuacionText == null)
        {
            Debug.LogError("�Falta asignar el objeto de texto UI en el inspector de Unity!");
        }
    }

    void Update()
    {

    }

    /// <summary>
    /// Esta funci�n incrementa _Puntuacion en uno y luego llama a la funci�n ActualizarTextoPuntuacion() para actualizar el valor del objeto puntuacionText.
    /// </summary>
    public void IncrementarPuntuacion()
    {
        
            _Puntuacion++;
        
       
        ActualizarTextoPuntuacion();
    }

    public void ReiniciarPuntuacion()
    {
        _Puntuacion = 0;
        ActualizarTextoPuntuacion();
    }


    /// <summary>
    ///  Esta funci�n actualiza el texto del objeto puntuacionText con el valor actual de _Puntuacion concatenado con la cadena "Puntuaci�n: ".
    /// </summary>
    private void ActualizarTextoPuntuacion()
    {
        if (puntuacionText != null)
        {
            puntuacionText.text = "Jugador 1: " + _Puntuacion.ToString() + "\nJugador 2: " + puntuacionJugador2.ToString();
        }
    }

}