using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class LineaScript : MonoBehaviour
{
    public GameObject puntoPrefab;
    public Material lineaMaterial;
    public float lineWidth = 0.1f;

    private List<GameObject> puntosConectados = new List<GameObject>();
    public LineRenderer lineRenderer;

    public Puntuacion PuntuacionScript;
    public PuntoScript PuntoScript;

    public List<GameObject> puntosOrdenados; //Lista de puntos en el orden correcto
    
    private int siguientePunto = 0; //�ndice del siguiente punto en el orden correcto
    private int puntuacionTotal = 0; // Puntuaci�n total del jugador
    private bool lineaCompleta = false;

    private List<GameObject> puntosDelGrupo;


    /// <summary>
    /// Esta funci�n se ejecuta al inicio del juego. Configura las propiedades del LineRenderer, como el material y el ancho de l�nea.
    /// </summary>
    private void Start()
    {
        InicializarLineas();
    }

    public void InicializarLineas()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material = lineaMaterial;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;
    }
    /*
    public void AsignarPuntos(GameObject grupoDePuntos)
    {
        puntosDelGrupo = new List<GameObject>(grupoDePuntos.GetComponentsInChildren<PuntoScript>());
    }*/

    /// <summary>
    /// Esta funci�n se llama cuando un punto es conectado correctamente. Actualiza la lista de puntos conectados, incrementa la puntuaci�n, actualiza el LineRenderer con los nuevos puntos conectados y verifica si se han conectado todos los puntos en el orden correcto. Si todos los puntos est�n conectados, se reinician los puntos.
    /// </summary>
    /// <param name="punto"></param>
    public void ConectarPunto(GameObject punto)
    {
        bool conexionCorrecta = true;

        if (puntosOrdenados[siguientePunto] == punto && punto.GetComponent<PuntoScript>())
        {
            puntosConectados.Add(punto);

            siguientePunto++;

            // Incrementar la puntuaci�n
            PuntuacionScript.IncrementarPuntuacion();
            puntuacionTotal++;

            // Actualizar el LineRenderer con los nuevos puntos conectados
            lineRenderer.positionCount = puntosConectados.Count;
            for (int i = 0; i < puntosConectados.Count; i++)
            {
                lineRenderer.SetPosition(i, puntosConectados[i].transform.position);
            }

            if (puntosConectados.Count == puntosOrdenados.Count)
            {
                ReiniciarPuntos();
                lineaCompleta = true;
            }
        }
        else
        {
            conexionCorrecta = false;
            Debug.Log("�NO SE CONECTARON CORRECTAMENTE LOS PUNTOS!");
            DeseleccionarTodosLosPuntos();
        }
        if (!conexionCorrecta)
        {
            DeseleccionarPuntos();
        }
    }

    /// <summary>
    /// Esta funci�n se llama cuando se corta la l�nea. Limpia la lista de puntos conectados, restablece la posici�n del LineRenderer y restablece el siguientePunto al valor inicial.
    /// </summary>
    public void CortarLinea()
    {
        puntosConectados.Clear();
        lineRenderer.positionCount = 0;

        siguientePunto = 0;

        lineRenderer.enabled = true;
    }

    /// <summary>
    /// Esta funci�n se llama cuando se han conectado todos los puntos en el orden correcto. Limpia la lista de puntos conectados, restablece la posici�n del LineRenderer y reposiciona y deselecciona cada punto en la lista de puntos ordenados.
    /// </summary>
    private void ReiniciarPuntos()
    {
        puntosConectados.Clear();
        lineRenderer.positionCount = 0;

        siguientePunto = 0;


        foreach (GameObject punto in puntosOrdenados)
        {
            PuntoScript puntoScript = punto.GetComponent<PuntoScript>();
            if (puntoScript != null)
            {
                puntoScript.ReposicionarPunto();
                puntoScript.Deseleccionar();
            }
        }
    }

    /// <summary>
    /// Esta funci�n se llama cuando los puntos conectados no est�n en el orden correcto. Deselecciona los puntos conectados y limpia la lista de puntos conectados.
    /// </summary>
    private void DeseleccionarPuntos()//NO DESELECCIONA TODO, SOLO QUEDA SELECCIONADO EL ULTIMO.
    {
        foreach (GameObject punto in puntosConectados)
        {
            PuntoScript puntoScript = punto.GetComponent<PuntoScript>();
            if (puntoScript != null)
            {
                puntoScript.Deseleccionar();
            }
        }
        puntosConectados.Clear();
    }

    /// <summary>
    /// Esta funci�n se llama cuando se desea deseleccionar todos los puntos, independientemente de si est�n conectados o no. Deselecciona todos los puntos conectados y limpia la lista de puntos conectados.
    /// </summary>
    private void DeseleccionarTodosLosPuntos()
    {
        foreach (GameObject punto in puntosConectados)
        {
            PuntoScript puntoScript = punto.GetComponent<PuntoScript>();
            if (puntoScript != null)
            {
                puntoScript.Deseleccionar();
            }
        }

        puntosConectados.Clear();
    }
}