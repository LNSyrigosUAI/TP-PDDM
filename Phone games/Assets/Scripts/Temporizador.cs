using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Temporizador : MonoBehaviour
{
    public float tiempoRestante = 60.0f;
    public Text textoTemporizador;
    private bool tiempoTerminado = false;
    public Image Perdiste;

    public Text textoPuntuacionFinal;

    public Puntuacion PuntuacionScript;

    /// <summary>
    /// Esta funci�n se ejecuta al inicio del juego. Llama a la funci�n
    /// </summary>
    void Start()
    {
        ActualizarTextoTemporizador();
    }

    /// <summary>
    /// Esta funci�n se ejecuta en cada fotograma del juego. Si el tiempo no ha terminado, decrementa tiempoRestante con el tiempo transcurrido desde el �ltimo fotograma y llama a ActualizarTextoTemporizador() para mostrar el tiempo restante actualizado en la interfaz de usuario. Si el tiempo llega a cero, se activa el indicador de "Perdiste" (asumiendo que se asigna un objeto de Image a Perdiste), se muestra la puntuaci�n final y se desactiva el temporizador.
    /// </summary>
    void Update()
    {
        if (!tiempoTerminado)
        {
            if (tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;
                ActualizarTextoTemporizador();
            }
            else
            {
                tiempoRestante = 0;
                tiempoTerminado = true;
                // Realiza cualquier acci�n que desees cuando el temporizador llega a cero
                Debug.Log("Perdiste");

                // Mostrar puntuaci�n final
                int puntuacionFinal = PuntuacionScript._Puntuacion; // Accede a la puntuaci�n directamente desde el script de puntuaci�n
                textoPuntuacionFinal.text = "Puntuaci�n Final: " + puntuacionFinal.ToString();
                Perdiste.gameObject.SetActive(true);
            }
        }

        /*
        if (!tiempoTerminado)
        {
            if (tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;
                ActualizarTextoTemporizador();
            }
            else
            {
                tiempoRestante = 0;
                tiempoTerminado = true;
                // Realiza cualquier acci�n que desees cuando el temporizador llega a cero
                Debug.Log("Perdiste");
                Perdiste.gameObject.SetActive(true);
            }
        }*/
    }

    /// <summary>
    /// : Esta funci�n actualiza el texto del objeto textoTemporizador con el tiempo restante formateado en minutos y segundos.
    /// </summary>
    void ActualizarTextoTemporizador()
    {
        int segundos = Mathf.Max(Mathf.FloorToInt(tiempoRestante % 60), 0);
        int minutos = Mathf.Max(Mathf.FloorToInt(tiempoRestante / 60), 0);

        textoTemporizador.text = string.Format("{0:0}:{1:00}", minutos, segundos);
    }
}
