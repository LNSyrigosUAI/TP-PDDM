using UnityEngine;

public class PuntoScript : MonoBehaviour
{
    public Material puntoSeleccionadoMaterial;
    public Light puntoLight;

    private Material puntoDefaultMaterial;
    private bool seleccionado = false;
    
    private LineaScript lineaScript;


    public Transform[] puntosDeReferencia;

    /// <summary>
    /// Esta funci�n se ejecuta al inicio del juego. En ella se obtiene el material por defecto del punto, se busca una instancia del script LineaScript en la escena, se desactiva inicialmente la luz del punto y se reposiciona el punto llamando a la funci�n ReposicionarPunto().
    /// </summary>
    private void Start()
    {
        puntoDefaultMaterial = GetComponent<Renderer>().material;
        lineaScript = FindObjectOfType<LineaScript>();

        puntoLight.enabled = false; // Inicialmente, la luz estar� apagada

        //ReposicionarPunto();

        //Para reposicionar el punto en una ubicaci�n aleatoria de los puntos de referencia
        int index = Random.Range(0, puntosDeReferencia.Length);
        transform.position = puntosDeReferencia[index].position;
    }

    /// <summary>
    ///Esta funci�n se ejecuta en cada fotograma del juego y se activa cuando se detecta un toque en la pantalla. Verifica si el toque comienza en el objeto, cambia su estado de selecci�n y su material, y activa o desactiva la luz asociada. Tambi�n se comunica con el script LineaScript para conectar o cortar la l�nea seg�n el estado de selecci�n.
    /// </summary>
    void Update()
    {
        for (int i =0;i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);//Obtiene el primer toque

            if (touch.phase == TouchPhase.Began)
            {
                //Convierte la posici�n del toque en un rayo en el mundo
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    //Comprueba si se ha tocado este objeto
                    if (hit.collider.gameObject == gameObject)
                    {
                        if (seleccionado)
                        {
                            seleccionado = false;
                            GetComponent<Renderer>().material = puntoDefaultMaterial;
                            lineaScript.CortarLinea();
                        }
                        else
                        {
                            seleccionado = true;
                            GetComponent<Renderer>().material = puntoSeleccionadoMaterial;
                            lineaScript.ConectarPunto(gameObject);
                        }

                        puntoLight.enabled = seleccionado; //Encender o apagar la luz seg�n si el punto est� seleccionado
                    }
                }
            }
        }
    }

    /// <summary>
    ///  Esta funci�n reposiciona el punto en una ubicaci�n aleatoria sin colisiones. Genera una posici�n aleatoria y la asigna a la transformaci�n del punto hasta que no se detecten colisiones con otros objetos en esa posici�n.
    /// </summary>
    public void ReposicionarPunto()
    {
        //Vector3 randomPosition = GenerarPosicionAleatoriaSinColision();
        //transform.position = randomPosition;

        int index = Random.Range(0, puntosDeReferencia.Length);
        transform.position = puntosDeReferencia[index].position;
    }

    /// <summary>
    ///  Esta funci�n genera una posici�n aleatoria para el punto y verifica si hay colisiones con otros objetos en esa posici�n. Si se detecta una colisi�n, genera una nueva posici�n y vuelve a verificar hasta encontrar una posici�n sin colisiones.
    /// </summary>
    /// <returns></returns>
    private Vector3 GenerarPosicionAleatoriaSinColision()
    {
        Collider[] colliders;
        bool colisionDetectada;

        do
        {
            Vector3 randomPosition = new Vector3(Random.Range(-8f, 8f), Random.Range(-3f, 3f), 0f);
            transform.position = randomPosition;

            colliders = Physics.OverlapSphere(transform.position, 1f);
            colisionDetectada = false;

            foreach (var collider in colliders)
            {
                if (collider.gameObject != gameObject)
                {
                    colisionDetectada = true;
                    break;
                }
            }
        }
        while (colisionDetectada);

        return transform.position;
    }

    /// <summary>
    /// Esta funci�n se utiliza para deseleccionar el punto. Restaura el material por defecto del punto, desactiva la luz y cambia el estado de selecci�n a falso.
    /// </summary>
    public void Deseleccionar()
    {
        seleccionado = false;
        GetComponent<Renderer>().material = puntoDefaultMaterial;
        puntoLight.enabled = false;
    }
}