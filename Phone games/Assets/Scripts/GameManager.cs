using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public LineaScript lineaPrefab; // Prefab de la l�nea
    public LineaScript lineaPrefab2;
    public int numeroDeGruposDePuntos = 2;

    private List<LineaScript> lineas = new List<LineaScript>();

    private void Start()
    {
        // Instanciar l�neas para diferentes grupos de puntos
        for (int i = 0; i < numeroDeGruposDePuntos; i++)
        {
            LineaScript nuevaLinea = Instantiate(lineaPrefab, transform.position, Quaternion.identity);
            LineaScript nuevaLinea2 = Instantiate(lineaPrefab2, transform.position, Quaternion.identity);
            nuevaLinea.InicializarLineas();
            nuevaLinea2.InicializarLineas();
            lineas.Add(nuevaLinea);
            lineas.Add(nuevaLinea2);
        }

    }
}
